using SmallTasks;

namespace TestProjectApp
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestReverseGivenFiveLetterWord()
        {
            string input = "Hello";
            string expected = "olleH";

            string actual = SmallTasks.SmallTasks.Reverse(input);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestReverseWithTwoLetterWord()
        {
            string input = "Hi";
            string expected = "iH";

            string actual = SmallTasks.SmallTasks.Reverse(input);

            Assert.AreEqual(expected, actual);
        }

    }
}